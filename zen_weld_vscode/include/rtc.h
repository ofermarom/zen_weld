
#include <Arduino.h>



#define REPORT_COUNTER_LOCATION 10

#define SCL 12
#define SDA 13

#define DS1307_CTRL_ID 0x68 
#define EPPROM_ID 0x50


#define EEPROM_REPORT_POINTER 10

#define EPPROM_REPORTS_OFFSET 100




#define SET_SDA   pinMode(SDA,INPUT);digitalWrite(SDA,HIGH);
#define CLR_SDA   pinMode(SDA,OUTPUT);digitalWrite(SDA,LOW);

#define SET_SCL   digitalWrite(SCL,HIGH);
#define CLR_SCL   digitalWrite(SCL,LOW);

#define SDA_RD digitalRead(SDA)




#define SDAOUT pinMode(SDA,OUTPUT)
#define SDAIN pinMode(SDA,INPUT)


#define SDA_VAL  digitalRead(SDA)


#define UINT unsigned int
#define UCHAR unsigned char

#define BEGIN_RD 1
#define BEGIN_WR 0


#define I2C_DELAY  delayMicroseconds(2)

void START_IC_(void);
void STOP_IC_(void);

#define START_IC  START_IC_()
#define STOP_IC  STOP_IC_()
#define SEND_BIT(c) if(c==1){SET_SDA;} else{ CLR_SDA ;}SET_SCL;CLR_SCL
#define CLK_P SET_SCL; I2C_DELAY;  CLR_SCL

#define DS1307SQW_ENB 0x10
#define SQW_1HZ 0
#define SQW_4KH 1
#define SQW_8KH 2
#define SQW_32KH 3

#define DS_CONTROL_REG DS1307SQW_ENB | SQW_4KH





















//#define UINT unsigned int
//#define UCHAR unsigned char

//#define BEGIN_RD 1
//#define BEGIN_WR 0


//#define I2C_DELAY  delayMicroseconds(2)








typedef struct {
uint8_t Second;
uint8_t Minute;
uint8_t Hour;
uint8_t Wday;
uint8_t Date;
uint8_t Month;
uint16_t Year;
} TimeSt;

typedef struct {
  uint16_t num; 
  uint8_t Minute;
  uint8_t Hour;
  uint8_t Wday;
  uint8_t Date;
  uint8_t Month;
  uint8_t Year;
  uint8_t dia;
  uint16_t duration;
  uint16_t temperature;
  TimeSt DateAndTime ;
}ReportStruct;




bool initrtc();
bool RtcWriteTime();
bool RtcReadTime();
uint8_t bcd2dec(uint8_t num);
int8_t dec2bcd(uint8_t num);

void IncReportCounter();
void InitreportsCounter();