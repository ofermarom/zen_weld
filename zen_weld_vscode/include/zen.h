
#include <Arduino.h>
#include <SoftwareSerial.h>


#define MOFA_TEST_FREQ 115
#define COUPLER63_TIME 
#define FITTING63_TIME 135

#define COUPLER50_TIME 
#define FITTING50_TIME 80

#define COUPLER40_TIME 
#define FITTING40_TIME 60

// Weld Frequancy set for each type of mofe

#define FITTING63_FREQ 89

#define FITTING50_FREQ 100

#define FITTING40_FREQ 100


//detection current level for each mofa in 12V 
#define MOFA_40 140
#define MOFA_50 90
#define MOFA_63 1000
#define NO_MOFA_CURRENT 60


#define ADC_TO_BAT_VOLATGE 0.145
#define OUT_CURRENT_FACTOR 1
#define OUTPUT_SAMPLE_FACTOR 0.65
#define TEMPERATURE_FACTOR 0.07
#define START_MAX_TEMPERATURE 65

#define DEFAULT_FREQ 115



#define PB_5  5

//376 7A limit 
#define CURRENT_PROTECION_LEVEL 680
//482


#define FAN_START_TEMP 25

//A3
#define HV_PWM 10
#define DRV_ENB A3
#define WELD_OFF 2
#define T_SENS  A2
#define FAN 6
#define BATTERY  A7
#define OUTPUT_CURRENT A1
#define OUTPUT_VOLATGE A6
#define ON_PB A4
//#define SMART_LED 2


#define NSCK 13
#define MISO 12
#define MOSI 11
#define EXPnCS 4


//IO EXPANDER OUTPUTS
#define EXTRA_LED1 0x01
#define MAIN_POWER_ON 0x08
#define EXTRA_LED2 0x02
#define BUZZER 0x80


// Externa OUTPUTS
#define EXLED1 0
#define EXLED2 1
#define EXLED3 2

#define WELCOME 0
#define SCANNING 1
#define CHOOSE 2
#define PRESSSTART 3
#define CLOSE 4
#define NODATA 5
#define CHOSE_DIA 6
#define CHOOSTYPE 7
#define PARAMETERS 8
#define INPROCESS 9
#define FINISHED 10
#define DATALOGGER 11
#define DATA 12
#define STOP 13
#define BATTERYDISP 14
#define LOWBATTERY 15
#define HITEMP 16
#define TIMESET 17


#define BATTERY_FULL 75
#define BATTERY_90 74
#define BATTERY_60 72
#define BATTERY_40 70
#define BATTERY_20 68
#define BATTERY_LOW 60

#define TUNE_START_CURRENT 720



#define EVENT_PRESSED 0x00
#define EVENT_REALEASE 0x01


#define ANY_MOFA_EXIST_CURRENT 7
#define BIG_MOFA_CURRENT 10

#define ON true
#define OFF false

#define LEDS_MODE_IDLE 0
#define LEDS_MODE_SACN 1
#define LEDS_MODE_SCAN_BLINK 2
#define LEDS_MODE_WELD 3
#define LEDS_MODE_WELD_BLINK 4
#define LEDS_MODE_END 5
#define LEDS_MODE_END_BLINK 6
#define LEDS_BLINK_SPEED 400


#define OVER_CURRENT 1023
#define UNDER_CURRENT 10

void SetIoExpander(uint8_t val);


void weld_function();
void TurnOfffout();
void Display_weld_time(uint16_t weldtime);
void Display_weld_freq(uint8_t freq);
void display_init();
void Hardware_tasks();
void display_tasks();
void Display_battery(int voltage);
void Display_current(int curreent);
void dispaly_volatge(int volatge);
void Display_temperature(int temp);
void ReadAnalog();
void Update_display();
void ActivateLowVolatgeMofa(bool stat);
void SetPowerMode(bool HiVolatge,bool on_off);
void SetExtraOutput(uint8_t ouput ,bool on_off);
void DisplayGotoPage(int page );
void tone_A( int freq);
void PowerLedsTask();
void  display_init();
void Weld_power(bool start);
void GetWeldtime();
void SetTime(bool up,uint8_t sel);
void DisplayTime();
void DisplayDate();
void SaveReport( uint16_t num,uint8_t dia,uint16_t temperature,uint16_t duration);
void DataLogerShow(int action);
typedef struct huliprm
{



uint8_t WeldSize ;
uint8_t active_screan ;
uint16_t WeldTime ;
uint8_t cool_interval ;
uint8_t Scan ;
uint8_t StartWeld;
uint16_t SysVolatge;
uint16_t  battery;
bool Coupler,WeldIsOn;
uint32_t lastmilis;
uint8_t rdst ,rdb;
uint8_t rpt ,cmdbuf[5];
uint8_t TouchPageNum ,TouchCompID,TouchEevntType;
uint8_t ReturnToPage;
int SystemCurrent ;
int Temperature ;
int Freq= DEFAULT_FREQ;
int fou= DEFAULT_FREQ;//182;//145;
uint8_t PowerLedsMode = 0;
uint8_t TimeSetSelect;
int ReportsCounter=0;
int StartDisplayLogPos =0;

}systemp;



