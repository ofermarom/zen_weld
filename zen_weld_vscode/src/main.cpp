
#include "zen.h"
#include "rtc.h"
extern systemp sysprm;



void setup() {
  
    pinMode(NSCK, OUTPUT); 
    pinMode(MOSI, OUTPUT); 
    pinMode(EXPnCS, OUTPUT); 
     
    pinMode(PB_5, INPUT_PULLUP); 

    pinMode(DRV_ENB, INPUT); // set as input
    digitalWrite(DRV_ENB,LOW);


    pinMode(HV_PWM, OUTPUT);
    digitalWrite(HV_PWM,LOW);
    
    pinMode(ON_PB,INPUT_PULLUP);
   
  
   // pinMode(SMART_LED,OUTPUT);
    pinMode(FAN,OUTPUT);
    digitalWrite(FAN,LOW);
    
    
    pinMode(WELD_OFF, INPUT); 
  //  display_init();
 
     display_init();
    
    Serial.begin(9600);

    SetIoExpander(0);

    SetExtraOutput(MAIN_POWER_ON,false );
    SetExtraOutput(BUZZER,false );
    SetExtraOutput(EXTRA_LED1,false );
    SetExtraOutput(EXTRA_LED2,false );

    sysprm.PowerLedsMode = LEDS_MODE_IDLE;
    sysprm.rdst = 0;
    sysprm.WeldSize =0;
    sysprm.WeldTime =0;
    sysprm.StartWeld =0;
    sysprm.cool_interval =0;
    sysprm.Scan =0;
    Serial.print("Ready \r\n");
    analogReference(EXTERNAL);
  //tone_A(WELD_OFF, 255-DEFAULT_FREQ); // set frequancy to 42khz 
    initrtc();
    DisplayGotoPage(WELCOME); 
    InitreportsCounter();
}

void loop() {
  Hardware_tasks();
  PowerLedsTask();
  display_tasks();
  weld_function();
}



void weld_function()
{
//char buff[20];
static int WeldDuration =0;
  if(sysprm.StartWeld == 1)
  {
        
      if( millis() >   sysprm.lastmilis)
      {
        
          sysprm.lastmilis = millis()+1000;
          
          if(sysprm.WeldTime)
          {
            //SetExtraOutput(MAIN_POWER_ON,true );                
            sysprm.WeldIsOn = true;
              sysprm.WeldTime--;      
              WeldDuration++;        
              if(sysprm.SystemCurrent > TUNE_START_CURRENT)// 85 8.5A
              {
               
                tone_A(255 - sysprm.Freq);
                Display_weld_freq(sysprm.Freq); 
                                                
              }      


              if(WeldDuration > 4)
              if(sysprm.SystemCurrent > OVER_CURRENT &&  sysprm.SystemCurrent < UNDER_CURRENT)
              {
                    sysprm.WeldTime = 0;
                    

              }

                    
                

           }else
           
           {
                DisplayGotoPage(FINISHED);
                sysprm.active_screan = FINISHED;

              if(sysprm.WeldIsOn)
              {
                GetWeldtime();
                SaveReport(sysprm.ReportsCounter,sysprm.WeldSize ,sysprm.Temperature , sysprm.WeldTime - WeldDuration);
                sysprm.WeldTime = WeldDuration = 0;
                IncReportCounter();
              }
                sysprm.StartWeld = 0;
                TurnOfffout();//digitalWrite(DRV_ENB,LOW);
                SetExtraOutput(MAIN_POWER_ON,false );                
                sysprm.WeldIsOn = false;
                sysprm.cool_interval = 60;
                sysprm.PowerLedsMode =   LEDS_MODE_SACN;
              
                
           }
      }
       
       
                
      }else if(sysprm.StartWeld == 2)
      {
        int Current =  analogRead(OUTPUT_CURRENT);
        if(Current > MOFA_63)
        {
          DisplayGotoPage( CHOOSTYPE);
          sysprm.WeldSize = 63;
          
        }else if(Current > MOFA_50)
        {
          sysprm.WeldSize = 50;
          DisplayGotoPage( CHOOSTYPE);
        }else if(Current > MOFA_40)
        {
          DisplayGotoPage( CHOOSTYPE);
          sysprm.WeldSize = 40;
        }
    
    
      }else
      {
        sysprm.WeldIsOn = false;
        SetPowerMode(true,false);

        if( millis() >   sysprm.lastmilis)
        {
          sysprm.lastmilis = millis()+1000;
          // now = rtc.now();
              //DispalyRtc();
        }

        
       
      }
    
      
      
    }
    
