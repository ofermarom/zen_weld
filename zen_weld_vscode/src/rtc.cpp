#include "zen.h"
#include "rtc.h"
#include <EEPROM.h>

TimeSt  CurTime ;
extern systemp sysprm;

void DisplaySendText(char *str );
void START_IC_(void)
{
   //PULL_UP_ENB;
   SET_SDA;
   SET_SCL;
   I2C_DELAY;
   CLR_SDA;  
   I2C_DELAY; 
   CLR_SCL;
   I2C_DELAY;
}

void STOP_IC_(void)
{
  CLR_SDA;
  I2C_DELAY;
  SET_SCL;
  I2C_DELAY;
  SET_SDA;  
  I2C_DELAY;
}



uint8_t ReadByte(bool ack)
{
  SDAIN;
   uint8_t tmp = 0x80,rd =0; 
   CLR_SCL;
   while (true)
   {
     SET_SCL;
     I2C_DELAY;
     if(SDA_RD)rd |= tmp;     
     CLR_SCL; 
     if(tmp == 0x1 )break;
     tmp >>=1;
   }
   
   if(ack){CLR_SDA;}
   SET_SCL;
   I2C_DELAY; 
   CLR_SCL;  
   CLR_SDA;
   return rd;
}

bool send_byte(uint8_t dbyte)
{
 uint8_t tmp = 0x80;
  bool result =false;
 CLR_SCL;
 while(1)
 {
  if(tmp & dbyte){SET_SDA;} else {CLR_SDA;}
  CLK_P;
  if(tmp == 0x1)break;
  tmp >>=1;
 }
  SDAIN;
  SET_SCL;
  I2C_DELAY; 
  result = (!SDA_VAL);
  CLR_SCL;
  
  return result;
}

bool beginTransmission(uint8_t id ,uint8_t nWR)
{
    START_IC_();
    return send_byte( id*2 |  nWR );
}

void endTransmission()
{
  STOP_IC_();
}

int y2kYearToTm(uint8_t y)
{
  return y+2000;
}

bool EepromWriteByte( uint8_t data, uint16_t start)
{
 bool result = true;
 if( beginTransmission(EPPROM_ID,BEGIN_WR))
       Serial.print("RTC Ok ");
 result &= send_byte( (start & 0xff00 )>>8);
 result &= send_byte((uint8_t) (start & 0xff));
 result &= send_byte( data);
 endTransmission();
return result;
}

bool EppromWrite(uint8_t *ibuffer,uint16_t start, uint16_t len)
{
 bool result = true;
 while(len)
 {
    result &= EepromWriteByte( *ibuffer++,start);
    len--;
 }
 return result;
}


bool EppromReadBuff(uint8_t *ibuffer,uint16_t start, uint16_t len)
{
   bool result = true;
   result &= beginTransmission(EPPROM_ID,BEGIN_WR);   
   result &= send_byte( (start & 0xff00 )>>8);
   result &= send_byte( start & 0xff );
   result &= beginTransmission(EPPROM_ID,BEGIN_RD);   
   while(len)
   {
      *ibuffer++= ReadByte(len > 1 ? true:false);
      len--;
   }
  endTransmission();
  
  return result ;
}



bool RtcReadTime()
{
  delay(10);
  beginTransmission(DS1307_CTRL_ID,BEGIN_WR);
  send_byte( (uint8_t)0x00 );
 // endTransmission();
  beginTransmission(DS1307_CTRL_ID,BEGIN_RD);
  CurTime.Second = bcd2dec(ReadByte(true) & 0x7f);  
  CurTime.Minute = bcd2dec(ReadByte(true));
  CurTime.Hour = bcd2dec(ReadByte(true));
  CurTime.Wday = bcd2dec(ReadByte(true));
  CurTime.Date = bcd2dec(ReadByte(true));
  CurTime.Month = bcd2dec(ReadByte(true));
  CurTime.Year = y2kYearToTm(bcd2dec (ReadByte(false)));
  endTransmission();
  return true;
}

uint8_t tmYearToY2k(int y)
{
  return  y-2000;
}


bool RtcWriteTime()
{
  bool exists = true;
  beginTransmission(DS1307_CTRL_ID,BEGIN_WR);

  Serial.print("RTC Write");
  Serial.print(CurTime.Hour);
   Serial.print("\r\n");
  exists &= send_byte((uint8_t)0x00); // reset register pointer  
  exists &=send_byte(CurTime.Second | (uint8_t)0x80); // Stop the clock. The seconds will be written last
  exists &=send_byte(dec2bcd(CurTime.Minute));
  exists &=send_byte(dec2bcd(CurTime.Hour));      // sets 24 hour format
  exists &=send_byte(dec2bcd(CurTime.Wday));   
  exists &=send_byte(dec2bcd(CurTime.Date));
  exists &=send_byte(dec2bcd(CurTime.Month));
  exists &=send_byte(dec2bcd(tmYearToY2k(CurTime.Year))); 
  exists &=send_byte(DS_CONTROL_REG);
  endTransmission();
  initrtc(); 
 return exists;
}


bool initrtc()
{
  bool exists = true;
  pinMode(SCL,OUTPUT);
  beginTransmission(DS1307_CTRL_ID,BEGIN_WR);
  exists &= send_byte((uint8_t)0x00); 
  exists &= send_byte((uint8_t)0x00); // write to sec 0 to start 
  endTransmission();
  return exists;
  
}
int8_t dec2bcd(uint8_t num)
{
  return ((num/10 * 16) + (num % 10));
}
uint8_t bcd2dec(uint8_t num)
{
  return ((num/16 * 10) + (num % 16));
}



union convert {
    ReportStruct report;
    uint8_t buffer[sizeof(ReportStruct)];
};

ReportStruct ReadReport( int Num)
{
    
   union convert converter;   
   EppromReadBuff(converter.buffer,Num*sizeof(ReportStruct) +EPPROM_REPORTS_OFFSET ,sizeof(ReportStruct) );   
   return converter.report; 
  
}


void WriteReport(ReportStruct Rpt, int Num)
{
   union convert converter;  
   converter.report = Rpt;
   EppromWrite(converter.buffer,Num*sizeof(ReportStruct) +EPPROM_REPORTS_OFFSET ,sizeof(ReportStruct)); 
}


void SaveReport( uint16_t num,uint8_t dia,uint16_t temperature,uint16_t duration)
{
    ReportStruct Rpt;
    Rpt.dia  = dia;  
    Rpt.duration = duration;
    Rpt.temperature = temperature;
    Rpt.DateAndTime = CurTime;
    Rpt.num = num;
    WriteReport(Rpt,num);
    char buff[30];

    sprintf(buff,"t4.txt=\"ZW103_%d-%d-%d_%d:%d_%d\"", CurTime.Date ,CurTime.Month,CurTime.Year,CurTime.Hour,CurTime.Minute,num);
    DisplaySendText(buff);
    
}





void IncReportCounter()
{
  sysprm.ReportsCounter++;
  EEPROM.write(REPORT_COUNTER_LOCATION, (sysprm.ReportsCounter>>8)&0xff ); 
  EEPROM.write(REPORT_COUNTER_LOCATION+1, sysprm.ReportsCounter&0xff ); 
}

void InitreportsCounter()
{
  sysprm.ReportsCounter = EEPROM.read(REPORT_COUNTER_LOCATION);
  sysprm.ReportsCounter <<= 8;
  sysprm.ReportsCounter += EEPROM.read(REPORT_COUNTER_LOCATION+1);
  if(sysprm.ReportsCounter > 0x7fff)sysprm.ReportsCounter=0;
  sysprm.StartDisplayLogPos = sysprm.ReportsCounter;
}






void DataLogerShow(int action)
{
    char buff[30];
    if(action == 1 && sysprm.StartDisplayLogPos)sysprm.StartDisplayLogPos--;
    if(action == 2 && sysprm.StartDisplayLogPos < sysprm.ReportsCounter)sysprm.StartDisplayLogPos++;

    int nlog = sysprm.StartDisplayLogPos,pos;
    
    for( pos=4;pos< 10;pos++)
    if(nlog <= sysprm.ReportsCounter)
    {
     ReportStruct Rpt = ReadReport(sysprm.StartDisplayLogPos);
     sprintf(buff,"t%d.txt=\"ZW-%d_%d-%d-%d_%d:%d\"",pos,nlog+1, Rpt.DateAndTime.Date ,Rpt.DateAndTime.Month,Rpt.DateAndTime.Year,Rpt.DateAndTime.Hour,Rpt.DateAndTime.Minute);
     DisplaySendText(buff);
     nlog++;
    }

    

}