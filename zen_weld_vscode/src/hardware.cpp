#include "zen.h"
#include <Arduino.h>


systemp sysprm;


void PowerLedsTask()
{

  static uint32_t LastTime =0;
  static uint8_t last_mode =30;
  static bool toogle = false;


  
  switch(sysprm.PowerLedsMode)
  {
      case LEDS_MODE_IDLE:
      SetExtraOutput(BUZZER,false);  
        if(sysprm.PowerLedsMode != last_mode )
        {
            SetExtraOutput(EXTRA_LED1,false );
            SetExtraOutput(EXTRA_LED2,false);
            
        }
  
  
      break;

      case LEDS_MODE_SACN:
          SetExtraOutput(BUZZER,false);
        if(sysprm.PowerLedsMode != last_mode )
        {
            SetExtraOutput(EXTRA_LED1,true);
            SetExtraOutput(EXTRA_LED2,false );
            
        }
  
      break;
  

      case LEDS_MODE_SCAN_BLINK:
       
        if(millis() > LastTime)
        {
           SetExtraOutput(EXTRA_LED1,false );
           SetExtraOutput(EXTRA_LED2,toogle);
           //SetExtraOutput(BUZZER,toogle);
           
            toogle = toogle? false: true;
            LastTime = ( millis() + LEDS_BLINK_SPEED);
            
        }
      
  
        
      break;

      case LEDS_MODE_WELD:
         SetExtraOutput(BUZZER,false);
        if(sysprm.PowerLedsMode != last_mode )
            {
                SetExtraOutput(EXTRA_LED1,false );
                SetExtraOutput(EXTRA_LED2,true );
                
            }
  
                
      break;

      case LEDS_MODE_WELD_BLINK:
          
          if(millis() > LastTime)
          {
             SetExtraOutput(EXTRA_LED1,toogle );
             SetExtraOutput(EXTRA_LED2,true );
             SetExtraOutput(BUZZER,toogle);
             
               toogle= toogle? false:true;
            LastTime =(  millis() + LEDS_BLINK_SPEED);
             
          }
  
      break;

      case LEDS_MODE_END:
         SetExtraOutput(BUZZER,false);
      if(sysprm.PowerLedsMode != last_mode )
        {
            SetExtraOutput(EXTRA_LED1,false );
            SetExtraOutput(EXTRA_LED2,true );
            
        }
         
      break;

  

   
  }   last_mode = sysprm.PowerLedsMode;
 

}

void ActivateLowVolatgeMofa(bool stat)
{

  if(stat )
  {
    tone_A(255- MOFA_TEST_FREQ);  
    SetPowerMode(false,true);
    sysprm.StartWeld = 2;
  }else
  {
    SetPowerMode(false,false);
    sysprm.StartWeld = 0;
  }

}





void Hardware_tasks()
{
    static uint32_t time=0;
    if( time < millis())
    {
        time = millis()+550;
        ReadAnalog();
        Update_display();
    }
}



void SetPowerMode(bool HiVolatge,bool on_off)
{
  if(HiVolatge)
  {

      digitalWrite(HV_PWM,LOW);
      SetExtraOutput(MAIN_POWER_ON,on_off); 
  
  }else
  {    
      SetExtraOutput(MAIN_POWER_ON,false);
      if(on_off)
      {
         digitalWrite(HV_PWM,HIGH); 
         tone_A(255-MOFA_TEST_FREQ);
      }
      else      
       {
         TurnOfffout();
         digitalWrite(HV_PWM,LOW);          
       }         
  }


}

void Weld_power(bool start)
{
    if(start)
    {
        tone_A(255- sysprm.Freq);
        sysprm.StartWeld = 1;
        SetPowerMode(true,true);

    }else
    {            
        sysprm.StartWeld = 0;
        SetPowerMode(true,false);
    }



}



void SetIoExpander(uint8_t val)
{

  uint8_t pos = 0x80;
   pinMode(NSCK,OUTPUT);
  digitalWrite(NSCK,LOW);
  digitalWrite(EXPnCS,LOW);
 // if(val & 0x8)
  //         digitalWrite(MOSI,HIGH);
  //    else
   //       digitalWrite(MOSI,LOW);
   pos = 0x80;        
  while(true)
  {    
    if((val & pos)> 0)
      digitalWrite(MOSI,HIGH);
    else
      digitalWrite(MOSI,LOW);     
    delayMicroseconds(10);
    digitalWrite(NSCK,HIGH);
    delayMicroseconds(10);
    digitalWrite(NSCK,LOW);
    
    if(pos == 0x1) break;    
    pos >>= 1;
    
  }
  
  digitalWrite(EXPnCS,HIGH);
  delayMicroseconds(10);
  digitalWrite(EXPnCS,LOW);
  
}

uint8_t external_out_stat = 0;
void SetExtraOutput(uint8_t output ,bool on_off)
{

   if(on_off)
     external_out_stat |= output;
   else
     external_out_stat &= ~output;

 
  SetIoExpander(external_out_stat);
}




void ReadAnalog()
{


  static int updline =0;
  float ft;
  int vtmp ;//=  analogRead(BATTERY);
 
switch(updline)
{
  case 0:
      vtmp =  analogRead(BATTERY);
      ft = ADC_TO_BAT_VOLATGE; // for battery volathe calulation 
      ft *= vtmp;
      vtmp = ft;
      if(sysprm.WeldIsOn == false)
          sysprm.battery = vtmp;
      updline = 2;
  break;
  case 1: 

    updline = 2;
  break;


  case 2:
    vtmp =  analogRead(OUTPUT_CURRENT);  
    ft =  OUT_CURRENT_FACTOR;
    ft *= vtmp;
    vtmp = ft;
    sysprm.SystemCurrent = vtmp;
    if(vtmp > CURRENT_PROTECION_LEVEL)
                      Weld_power(false);        
    updline = 3;
  break;

  case 3:
    vtmp =  analogRead(OUTPUT_VOLATGE);
    ft = OUTPUT_SAMPLE_FACTOR;
    ft *= vtmp;
    vtmp = ft;
    sysprm.SysVolatge = vtmp;
    updline = 5;
  break;
  case 5:
    vtmp = analogRead(T_SENS);    
    ft = TEMPERATURE_FACTOR;
    ft *= vtmp;
    vtmp = ft;
    
    if(vtmp > FAN_START_TEMP)
    {
        digitalWrite(FAN,HIGH);
      
    }
    else if(vtmp < (FAN_START_TEMP-3))
    {
          digitalWrite(FAN,LOW);

    }
    //Serial.print(sysprm.Temperature);
    //Serial.print("\r\n");
    sysprm.Temperature = vtmp;

  updline = 0;
  break;


default: updline = 0;
break;


}
}


void ReadRTC()
{
//RTC_time
  
}


void TurnOfffout() // turn off frequnacy and driver 
{
  Serial.print('A');
  Serial.print('S');  
  Serial.print('Z');
  Serial.print('\r');
  Serial.print('\n');
}



void tone_A( int freq)
{
  Serial.print('A');
  Serial.print('S');
  Serial.print(freq);
  Serial.print('X');
  Serial.print('\r');
  Serial.print('\n');
}