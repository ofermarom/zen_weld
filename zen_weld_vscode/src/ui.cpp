
#include "zen.h"
#include "rtc.h"

extern TimeSt  CurTime ;
SoftwareSerial lcdport(7, 3); // RX, TX
extern systemp sysprm;
byte buf=0xff;
void update_filed(const char *fild , int value)
{
    lcdport.print(fild);
    lcdport.print(value);
    lcdport.write( buf);
    lcdport.write( buf);
    lcdport.write( buf);

}


void UpdateDateTag( char *str)
{
  lcdport.write("Date.txt=\"");
  while(*str)
       lcdport.write(*str++);
    lcdport.write("\"");    
    lcdport.write( buf);
    lcdport.write( buf);
    lcdport.write( buf);
  
}

void UpdateTimeTag (char *str)
{
 lcdport.write("Time.txt=\"");
  while(*str)
        lcdport.write(*str++);
    lcdport.write("\"");    
    lcdport.write( buf);
    lcdport.write( buf);
    lcdport.write( buf);
  
}


void UpdateTextTag(int tag,const char *str)
{  
    lcdport.write("t");
    lcdport.write(tag);
    lcdport.write(".txt=\"");
    while(*str)
        lcdport.write(*str++);
    lcdport.write("\"");    
    lcdport.write( buf);
    lcdport.write( buf);
    lcdport.write( buf);
}

void UpdateValTag(int tag,int val)
{  
    lcdport.write("n");
    lcdport.print(tag);
    lcdport.write(".val=");
    lcdport.print(val);
    lcdport.write( buf);
    lcdport.write( buf);
    lcdport.write( buf);
}



void Display_weld_time(uint16_t weldtime)
{
    lcdport.print("n6.val=");
    lcdport.print(weldtime);
    lcdport.write( buf);
    lcdport.write( buf);
    lcdport.write( buf);
}


void Display_weld_freq(uint8_t freq)
{
    lcdport.print("n3.val=");
    lcdport.print(freq);
    lcdport.write( buf);
    lcdport.write( buf);
    lcdport.write( buf);
}

void display_init()
{
     lcdport.begin(9600);
     
}




void Display_battery(int voltage)
{
    uint8_t batt_stat =30,level = 0;
    if(voltage > BATTERY_FULL)
      {  batt_stat = 25;  level =100; }
     else if(voltage > BATTERY_90)    
      {  batt_stat = 26;  level =80; }
     else if(voltage > BATTERY_60)    
      {  batt_stat = 27;  level =60; }
     else if(voltage > BATTERY_40)    
      {  batt_stat = 28;  level =40; }
    else if(voltage > BATTERY_20)    
      {  batt_stat = 29;  level =20; }
     else  
      {  batt_stat = 30;  level =5; }

   
         update_filed("p4.pic=" ,batt_stat);
         update_filed("Battry.val=" ,level);   
}


void Progress_show(int curreent)
{
  lcdport.print("n0.val=");
  lcdport.print(curreent);
  lcdport.write( buf);
  lcdport.write( buf);
  lcdport.write( buf);
}



void Display_current(int curreent)
{
  lcdport.print("n1.val=");
  lcdport.print(curreent);
  lcdport.write( buf);
  lcdport.write( buf);
  lcdport.write( buf);
}

void dispaly_volatge(int volatge)
{
  lcdport.print("n2.val=");
  lcdport.print(volatge);
  lcdport.write( buf);
  lcdport.write( buf);
  lcdport.write( buf);    

}
void Display_temperature(int temp)
{
  update_filed("Temp.val=" ,temp);
}


#define DEBUG_PAGE




void TouchProc()
{

 #ifdef DEBUG_PAGE 
   Serial.print( "Page ");
   Serial.print( sysprm.TouchPageNum);
   Serial.print("  - ");
   Serial.print( sysprm.active_screan);
   Serial.print(sysprm.TouchCompID);
   Serial.print( "\r\n");
 #endif   
  
  switch(sysprm.TouchPageNum)
  { 
    
      case WELCOME: 
          sysprm.PowerLedsMode =   LEDS_MODE_WELD;

          if(sysprm.TouchCompID == 0x01 && sysprm.TouchEevntType == EVENT_REALEASE)
            {
                sysprm.active_screan =  CHOSE_DIA;
                DisplayGotoPage(CHOSE_DIA);
                ///sysprm.PowerLedsMode =   LEDS_MODE_WELD_BLINK;
                ///ActivateLowVolatgeMofa(true);
                Serial.print("Active Mofa Current ");
                
            }

           else if(sysprm.TouchCompID == 12 )//&& sysprm.TouchEevntType == EVENT_REALEASE)
            {
              sysprm.active_screan =  TIMESET;
              DisplayGotoPage(TIMESET);
              Serial.print("Timeset \r\n ");
            }
          else if(sysprm.TouchCompID ==  2)
          {
            sysprm.active_screan =  DATALOGGER;
             DisplayGotoPage(DATALOGGER);  
             DataLogerShow(0);
          }
      
          break;
       case SCANNING:
        
          if(sysprm.TouchCompID == 0x02 && sysprm.TouchEevntType == EVENT_REALEASE )  
          {  
             ActivateLowVolatgeMofa(false);
             sysprm.active_screan =  WELCOME;
             DisplayGotoPage(WELCOME);

          }
          if(sysprm.TouchCompID == 0x12 && sysprm.TouchEevntType == EVENT_REALEASE )  
          {
            ActivateLowVolatgeMofa(false);
            sysprm.active_screan =  WELCOME;
            DisplayGotoPage(WELCOME);
            
          }

          if(sysprm.TouchCompID ==  1)
          {
            sysprm.active_screan =  DATALOGGER;
             DisplayGotoPage(DATALOGGER);
             DataLogerShow(0);  
          }
        break; 
        case CHOSE_DIA:
            if(sysprm.TouchCompID == 0x3 && sysprm.TouchEevntType == EVENT_REALEASE)sysprm.WeldSize = 40;
            if(sysprm.TouchCompID == 0x4 && sysprm.TouchEevntType == EVENT_REALEASE)sysprm.WeldSize = 50;
            if(sysprm.TouchCompID == 0x5 && sysprm.TouchEevntType == EVENT_REALEASE)sysprm.WeldSize = 63;
              DisplayGotoPage(PRESSSTART);
             sysprm.active_screan = PRESSSTART;
        break; 
          
      
        case PRESSSTART:
             delay(50);
            if((sysprm.TouchCompID == 5 ||  sysprm.TouchCompID == 20) && sysprm.TouchEevntType == EVENT_REALEASE) // && sysprm.Temperature < START_MAX_TEMPERATURE)
            {            
                GetWeldtime();                                          
                Weld_power(true);    
                sysprm.StartWeld = 1;
                sysprm.PowerLedsMode =   LEDS_MODE_IDLE;           
                DisplayGotoPage(INPROCESS);           
                sysprm.active_screan = INPROCESS;
                
            }
            else if(sysprm.TouchCompID == 15 && sysprm.TouchEevntType == EVENT_REALEASE)
            {
                sysprm.StartWeld = 0;
                TurnOfffout();
                digitalWrite(HV_PWM,LOW);
                SetPowerMode(true,false);
                DisplayGotoPage(WELCOME);
                sysprm.active_screan = WELCOME;
                
              
            }
            break; 
        case INPROCESS:
         
          if(sysprm.TouchCompID == 1 && sysprm.TouchEevntType == EVENT_REALEASE)
          {
            sysprm.StartWeld = 0;
            TurnOfffout();//digitalWrite(DRV_ENB,LOW);
            digitalWrite(HV_PWM,LOW);
            SetPowerMode(true,false);
            sysprm.PowerLedsMode =   LEDS_MODE_SACN;
            DisplayGotoPage(STOP);
            sysprm.active_screan = STOP;
            
            
            
       
          }
        break;
       case STOP:
        
          if(sysprm.TouchCompID == 13 && sysprm.TouchEevntType == EVENT_REALEASE )
          {  
            
              DisplayGotoPage(WELCOME); 
              sysprm.active_screan =  WELCOME;
              
              
    
          }
        break;

      case FINISHED:
      
      
        if(sysprm.TouchCompID == 0x04 && sysprm.TouchEevntType == EVENT_REALEASE) // new welding 
        {
            sysprm.active_screan = PRESSSTART;
            DisplayGotoPage(PRESSSTART);
            sysprm.PowerLedsMode =   LEDS_MODE_SCAN_BLINK; 
            
          
        }
        if(sysprm.TouchCompID == 8 && sysprm.TouchEevntType == EVENT_REALEASE)
        {
           
            DisplayGotoPage(WELCOME);
            sysprm.active_screan = WELCOME;
            sysprm.PowerLedsMode =   LEDS_MODE_WELD;
        }
            

          if(sysprm.TouchCompID ==  1)
          {
            sysprm.active_screan =  DATALOGGER;
             DisplayGotoPage(DATALOGGER);  
             DataLogerShow(0);
          }

     
      break;

      case TIMESET:
      
        if(sysprm.TouchEevntType == EVENT_REALEASE)
        switch(sysprm.TouchCompID)
        {
          case 2:  sysprm.TimeSetSelect = 1;  break;
          case 3:  sysprm.TimeSetSelect = 2;  break;
          case 5:  sysprm.TimeSetSelect = 3;  break;
          case 8:  sysprm.TimeSetSelect = 4;  break;
          case 9:  sysprm.TimeSetSelect = 5;  break;
          case 11: SetTime(true,sysprm.TimeSetSelect); break; 
          case 12: SetTime(false,sysprm.TimeSetSelect); break; 
          case 15:  sysprm.active_screan = WELCOME; break;

          
          
        }
        case DATALOGGER:
           if(sysprm.TouchEevntType == EVENT_REALEASE )
           {
              if(sysprm.TimeSetSelect == 19) DataLogerShow(1);
              if(sysprm.TimeSetSelect == 20) DataLogerShow(2);

           }


        break;


      break; 



  
  }




}






void display_tasks()
{
    
    
    if( Serial.available())    //    lcdport.available())//    
    {
        sysprm.rdb = Serial.read();//  lcdport.read();
          //Serial.write(sysprm.rdb);
        switch(sysprm.rdst)
        {
            case 0: if(sysprm.rdb == 0x65)sysprm.rdst++;     break; 
            case 1: sysprm.rdst++;  sysprm.TouchPageNum = sysprm.rdb;   break;
            case 2: sysprm.rdst++;  sysprm.TouchCompID = sysprm.rdb;   break;
            case 3: sysprm.rdst++; sysprm.TouchEevntType = sysprm.rdb;  break; /*  Serial.print(rdb);*/  
            case 4: if(sysprm.rdb == 0xff) TouchProc();   sysprm.rdst = 0; break; 
            default: sysprm.rdst =0;
        }
    }

  if(sysprm.rdst == 0)
          Update_display();

   
          

}
void DisplaySendText(char *str )
{
   while(*str)lcdport.print(*str++);
  
  lcdport.write( buf);
  lcdport.write( buf);
  lcdport.write( buf);

  
}
void CloseMsg()
{
 lcdport.write( buf);
  lcdport.write( buf);
  lcdport.write( buf);
  
}


void DisplayGotoPage(int page )
{  
  if(page != 7){
  lcdport.print("page ");
  lcdport.print(page);
  lcdport.write( buf);
  lcdport.write( buf);
  lcdport.write( buf);}
}

void GetWeldtime()
{
   sysprm.WeldTime = sysprm.Coupler = false;
  switch(sysprm.WeldSize)
  {
   
      case 63: sysprm.WeldTime =  FITTING63_TIME; sysprm.Freq =  FITTING63_FREQ; break;
      case 50: sysprm.WeldTime =  FITTING50_TIME; sysprm.Freq =  FITTING50_FREQ; break;
      case 40: sysprm.WeldTime =  FITTING40_TIME; sysprm.Freq =  FITTING40_FREQ; break;
      default: sysprm.WeldTime = 0; 
      break;
  }
}


void SetTime(bool up,uint8_t sel)
{
   
    RtcReadTime();
    switch(sel)
    {
      case 1:  if(up &&  CurTime.Year < 2100 )CurTime.Year++; if(up==false &&  CurTime.Year > 2010)CurTime.Year--;  break;
      case 2:  if(up &&  CurTime.Month < 12)CurTime.Month++;  if( up == false &&  CurTime.Month > 1)CurTime.Month--; break;
      case 3:  if(up && CurTime.Date < 31)CurTime.Date++; if (up == false &&  CurTime.Date > 1)CurTime.Date--;   break;
      case 4:  if(up && CurTime.Hour < 23)CurTime.Hour++; if (up == false &&  CurTime.Hour > 1)CurTime.Hour--;    break;
      case 5:  if(up && CurTime.Minute < 59)CurTime.Minute++; if (up == false &&  CurTime.Minute > 0)CurTime.Minute--;   break;
    } 

    RtcWriteTime();
}   







void Update_display()
{
    char buff[30];
    
    static uint32_t Stime=0;
    static uint8_t tdisplay =0;
    static uint8_t ldisplay =0;//,PbFilter =0;
    
    if(millis() > Stime && sysprm.active_screan != TIMESET)
    {
			
          Stime = millis() + 1000;//   sysprm.WeldIsOn ? 100 : 500;
          Serial.print( tdisplay);
          Serial.print("-");
          Serial.print( sysprm.active_screan);

         
          if(ldisplay == 0)
          {
            Display_battery(sysprm.battery);
            ldisplay = 1;
          }
           if(ldisplay == 1)
          {
              Display_temperature( sysprm.Temperature);
              ldisplay = 2;
          }
          if(ldisplay==2)
          {
            ldisplay=0;
              if(sysprm.cool_interval)
            {
              update_filed("p5.pic=" ,45); 
              sysprm.cool_interval--;
              
            }
            else{
            update_filed("p5.pic=" ,44);
          }
          }sysprm.WeldIsOn = true;


          DisplayTime();
          DisplayDate();
 
    }
          
          
          
          
         
        

          
                      
               

        
          switch(sysprm.active_screan)
          {
              
              case WELCOME:   
                  sysprm.PowerLedsMode =   LEDS_MODE_WELD;
                  if(digitalRead(  ON_PB)== 0)
                  {  
                      ActivateLowVolatgeMofa(false);
                      DisplayGotoPage(CHOSE_DIA);
                      sysprm.active_screan =  CHOSE_DIA;
                      sysprm.PowerLedsMode =   LEDS_MODE_SCAN_BLINK;
                      

                  }
                break; 
              case SCANNING:
                  ActivateLowVolatgeMofa(true);
                  sysprm.Scan = ((sysprm.SystemCurrent)*10);
                  update_filed("n0.val=" ,sysprm.Scan);
                 if(sysprm.Scan > NO_MOFA_CURRENT)
                    {
                       

                      if(sysprm.Scan >= (MOFA_50-10) && sysprm.Scan < (MOFA_50+10))
                      { 
                        sysprm.WeldSize = 50;
                        sysprm.PowerLedsMode =   LEDS_MODE_WELD_BLINK;
                        DisplayGotoPage(PRESSSTART);
                        sysprm.active_screan = PRESSSTART;
                        
                        //sysprm.Coupler = true;
                        
                        ActivateLowVolatgeMofa(false);        
                      }else if(sysprm.Scan >= (MOFA_40-10) && sysprm.Scan < (MOFA_40+10))
                      { 
                        sysprm.WeldSize = 40;
                        sysprm.PowerLedsMode =   LEDS_MODE_WELD_BLINK;
                        DisplayGotoPage(PRESSSTART);
                        sysprm.active_screan = PRESSSTART;
                        //sysprm.Coupler = true;
                        
                        ActivateLowVolatgeMofa(false); 
                      }else
                      { // mofa 63 
                     //   sysprm.active_screan = PRESSSTART;
                      //  sysprm.WeldSize = 63;
                       // DisplayGotoPage(PRESSSTART);
                      }
                          
                                   
                   
                    }
                 break; 
              case INPROCESS:
                
                 Progress_show(sysprm.WeldTime);
                  
                  switch(tdisplay)
                  {
                    case 0: tdisplay = 3; break;
                    case 1: tdisplay = 3; break;
                    case 2: Progress_show(sysprm.WeldTime);  tdisplay = 3; break; 
                    case 3:  Display_temperature(sysprm.Temperature);tdisplay = 4; break;
                    case 4:  Display_current(sysprm.SystemCurrent);tdisplay = 5; break;
                    case 5: dispaly_volatge(sysprm.SysVolatge);tdisplay = 2; break;
                    default:tdisplay = 2; break;
                    
                  }
                  
                
                break;
              
              case PARAMETERS:
                
               break;
              case  PRESSSTART:
                sysprm.PowerLedsMode =   LEDS_MODE_WELD_BLINK;
                delay(200);
                if(sysprm.Temperature > START_MAX_TEMPERATURE)
                {
                   
                   DisplayGotoPage(HITEMP);     
                   sysprm.active_screan = HITEMP;
                   sysprm.PowerLedsMode =   LEDS_MODE_SACN;                       
                  
                }
                else if( sysprm.Temperature > 65)
                {
                                
                   sprintf(buff,"t11.txt=\"WAIT FOR COOL DOWN°\"");
                   DisplaySendText(buff);
                }
               
		            else if(digitalRead( ON_PB)== 0)
                 {   
                      GetWeldtime();  
                      Weld_power(true);
                      sysprm.PowerLedsMode =   LEDS_MODE_WELD;
                  
                      Serial.print("Button Was Press");
                       
                      DisplayGotoPage(INPROCESS);
                      sysprm.active_screan = INPROCESS;               
                     
                    
                 } 

                 else
				         {
                
                   GetWeldtime();
                   sprintf(buff,"t3.txt=\"%d\"", sysprm.WeldSize);                     
                   DisplaySendText(buff);
                   update_filed("p0.pic=",23);
                   if(sysprm.Coupler)
                   sprintf(buff,"t0.txt=\"Coupler\"");
                   else
                   sprintf(buff,"t0.txt=\"fitting\"");
                   DisplaySendText(buff);
        
                   sprintf(buff,"t7.txt=\"%d Sec\"", sysprm.WeldTime);
                   DisplaySendText(buff);
                  
                   sprintf(buff,"t8.txt=\"%d°\"", sysprm.Temperature);
                   DisplaySendText(buff);
                   sprintf(buff,"t11.txt=\"REDAY°\"");
                   DisplaySendText(buff);
                                        
				         }
                
                   
                
      
              break;
    
             case STOP:
            
              if(sysprm.TouchCompID == 13 && sysprm.TouchEevntType == EVENT_REALEASE )
              {  
                 DisplayGotoPage(WELCOME);
                 sysprm.active_screan = WELCOME;     
                 
                  
        
              }
             break;
        
              case FINISHED:
                  sysprm.PowerLedsMode =   LEDS_MODE_WELD;  

                  

                  if(digitalRead( ON_PB)== 0)
                 {   
                      
                      DisplayGotoPage(PRESSSTART);
                      sysprm.active_screan = PRESSSTART;               
                      
                       
                 } 
                  if(sysprm.TouchCompID == 0x8 && sysprm.TouchEevntType == EVENT_REALEASE)
                {
                   
                    DisplayGotoPage(WELCOME);
                    sysprm.active_screan = WELCOME;
                    sysprm.PowerLedsMode =   LEDS_MODE_WELD;
                }


                    
             break;
               
              case HITEMP:
                  sysprm.PowerLedsMode = LEDS_MODE_END;
                   if(sysprm.Temperature < START_MAX_TEMPERATURE)
                   {
                      DisplayGotoPage(sysprm.ReturnToPage);
                      sysprm.active_screan = sysprm.ReturnToPage;
                    
                   }



              break;
        
              
      
        
              case TIMESET:
                  RtcReadTime();
                  UpdateValTag(0,CurTime.Year);
                  UpdateValTag(1,CurTime.Month);
                  UpdateValTag(2,CurTime.Date);
                  UpdateValTag(3,CurTime.Hour);
                  UpdateValTag(4,CurTime.Minute);
                  UpdateValTag(5,CurTime.Second);
                  delay(300);

              break;
      
      
          }//case 
        
      }
    
    


void DisplayTime()
{ 
  byte buf=0xff;
  int i=0;
  char str[20];
  RtcReadTime();
  sprintf(str,"Time.txt=\"%d:%d:%d\"",CurTime.Hour,CurTime.Minute ,CurTime.Second);
  while(str[i])
        lcdport.write( str[i++]);

  lcdport.write( buf);
  lcdport.write( buf);
  lcdport.write( buf);
}

void DisplayDate()
{
  int i=0; 
  byte buf=0xff;
  char str[20];
  sprintf(str,"Date.txt=\"%d-%d-%d\"",CurTime.Date,CurTime.Month ,CurTime.Year);
 while(str[i])
        lcdport.write( str[i++]);

  lcdport.write( buf);
  lcdport.write( buf);
  lcdport.write( buf);
}